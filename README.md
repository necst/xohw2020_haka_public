# PROGETTO INDICATORE DI FOURIER PER HFT #

### Obiettivi ###


Fornire un nuovo indicatore per la il trading di HFT in modo da poterlo avere direttamente all’interno di una FPGA.
Un futuro cliente potrà quindi istanziare il nostro IP direttamente sulla propria FPGA che si trova collegata all’Exchange.

### Kernel ###

All’interno del nostro kernel abbiamo 3 diversi in ingressi per la prima versione.
Avremo il vettore con i dati del flusso del prezzo (vettore di float) come primo ingresso. 
Come secondo ingresso abbiamo il numero totale di record (numero di record del titolo) e infine abbiamo il terzo ingresso che determina come viene sviluppata la trasformata di Fourier che la chiameremo t_sampling.

In uscita avremo un vettore lungo esattamente come il vettore in entrata.

### Algoritmo ###

L’algoritmo si sviluppa con la prima trasformata dove useremo la formulazione per funzioni pari e reali. In questo modo potremo usare il coseno al posto dell’esponenziale complesso.
Una volta ultima la trasformata useremo la proprietà della trasformata di Fourier. Applicando la trasformata di Fourier alla trasformata otterremo i dati iniziali.

### Idea alla base dell'algoritmo ###

Si vuole ottenere un indicatore che sia in grado di definire un possibile trend rialzista o ribassista. Perché usare questo algoritmo? 
Usando due volte la trasformata di Fourier siamo in grado di perdere informazioni dai dati iniziali e restituire in uscita un’indicazione generale del trend del prezzo. 
Usando una trasformata di Fourier discreta non saremo in grado di ottenere i dati iniziali ma useremo questo limite a nostro vantaggio.


# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact