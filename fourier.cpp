#include "string.h"
#include "ap_int.h"
#include <math.h>
#define PI 3.14159265358979323846
#define DATA_SIZE 2000
extern "C" {
void fourier(float *data,float *result,const int scala,const int n_armoniche){
//interfaces for the master axi communication
#pragma HLS INTERFACE m_axi depth=2000 port=data bundle=gmem0
#pragma HLS INTERFACE m_axi depth=2000 port=result bundle=gmem1

//interfaces for the addresses of a, b, and c pointers
#pragma HLS INTERFACE s_axilite port=data			bundle = control
#pragma HLS INTERFACE s_axilite port=result			bundle = control
#pragma HLS INTERFACE s_axilite port=scala			bundle = control
#pragma HLS INTERFACE s_axilite port=n_armoniche	bundle = control
#pragma HLS INTERFACE s_axilite port=return			bundle = control

	//ora dichiaro le variabili
	float trasformation_coseno[DATA_SIZE];
	float trasformation_seno[DATA_SIZE];
	float output[DATA_SIZE];
	float flip[DATA_SIZE];


	float somma_coseno = 0;
	float somma_seno = 0;
	float dx = 1;
	float valore_base = 0;
	float somma_finale = 0;


	//fine dichiarazione delle variabili

	for(int i = 0; i < scala; i++){
#pragma HLS pipeline
		flip[i] = data[i];
	}

	dx = (dx/scala);


	for(int y = 0; y < n_armoniche; y++){
		somma_coseno = 0;
		somma_seno = 0;
#pragma HLS pipeline
		for(int o = 0; o < scala; o++){
#pragma HLS pipeline
			somma_coseno += flip[o]*cos(2*PI*(o*dx)*(y));
			somma_seno += flip[o]*sin(2*PI*(o*dx)*(y));
		}

		trasformation_coseno[y] = 2*somma_coseno*dx;
		trasformation_seno[y] = 2*somma_seno*dx;
	}

	valore_base = trasformation_coseno[0];

	//utilizzare un array lungo k=ii e poi indicizzato con o%k per ogni accumulazione
	//ap_fixed

	for(int i = 0; i < scala; i++){
#pragma HLS pipeline
		somma_finale = 0;

		for(int u = 1; u < n_armoniche;u++){
#pragma HLS pipeline
			somma_finale += (trasformation_coseno[u]*cos(u*2*PI*i*dx) + trasformation_seno[u]*sin(u*2*PI*i*dx));
		}

		output[i] = valore_base/2 + somma_finale;
	}



	for(int i = 0; i < scala; i++){
#pragma HLS pipeline
		result[i] = output[i];
	}

}
}
